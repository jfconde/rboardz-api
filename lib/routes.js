const Router = require('express').Router;
const usersController = require('./controllers/users/controller');
const userController = require('./controllers/user/controller');
const authController = require('./controllers/auth/controller');

const fn = (routes) => (config = {}) => {
    const {
              apiPath         = '/api',
              authPath        = '/auth',
              authCallbackURL = '/:provider/callback',
              redirectUri     = '/'
          } = config || {};

    const router = Router({mergeParams: true});

    routes.forEach(r => {
        if (r.method) {
            router[r.method](`${apiPath}${r.path}`, r.handler);
        } else {
            router.use(`${apiPath}${r.path}`, r.handler);
        }
    });

    // Add authorization routes if present
    if (authPath && authCallbackURL) {
        router.use(authPath, authController(authCallbackURL, redirectUri));
    }

    return router;
};

const routes = [
    {path: '/user', handler: userController(), method: null},   // method: null === all methods (use())
    {path: '/users', handler: usersController(), method: null}
];

module.exports = fn(routes);