const Router = require('express').Router;
const db = require('../../db/models');
const withApiResponse = require('../../helpers/apiResponse').withApiResponse;

const controller = () => {
    router = new Router({mergeParams: true});

    router.get('/', withApiResponse(async (req) => {
        if (!req.user) {
            return {error: {code: 'UNAUTHORIZED'}};
        }
        const users = await db.User.findAll();
        return {
            data: users
        };
    }));

    return router;
};



module.exports = controller;