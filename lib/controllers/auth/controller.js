const Router = require('express').Router;
const passport = require('passport');
const uuid = require('uuid');

const twitterCallback = (redirectUri) => (req, res) => {
    const io = req.app.get('io');
    const user = {
        name : req.user.username,
        photo: req.user.photos[0].value.replace(/_normal/, '')
    };
    io.in(req.session.socketId).emit('twitter', user);
    res.redirect(redirectUri);
};

const googleCallback = (redirectUri) => (req, res) => {
    const io = req.app.get('io');
    const user = {
        name : req.user.displayName,
        photo: req.user.photos[0].value.replace(/sz=50/gi, 'sz=250')
    };
    io.in(req.session.socketId).emit('google', user);
    res.redirect(redirectUri);
};

const facebookCallback = (redirectUri) => (req, res) => {
    const io = req.app.get('io');
    const {givenName, familyName} = req.user.name;
    const user = {
        name : `${givenName} ${familyName}`,
        photo: req.user.photos[0].value
    };
    io.in(req.session.socketId).emit('facebook', user);
    res.redirect(redirectUri);
};

const githubCallback = (redirectUri) => (req, res) => {
    const io = req.app.get('io');
    const user = {
        name : req.user.username,
        photo: req.user.photos[0].value
    };
    io.in(req.session.socketId).emit('github', user);
    res.redirect(redirectUri);
};

const providers = [
    {enabled: true, name: 'twitter', authenticate: passport.authenticate('twitter'), callback: twitterCallback},
    {
        enabled: true, name: 'google', authenticate: passport.authenticate('google', {scope: ['profile']}), callback: googleCallback,
    },
    {enabled: true, name: 'facebook', authenticate: passport.authenticate('facebook'), callback: facebookCallback},
    {enabled: true, name: 'github', authenticate: passport.authenticate('github'), callback: githubCallback}
];

const getController = (callbackURL, redirectUri) => {
    const router = Router();
    // Setting up the passport middleware for each of the OAuth providers
    const enabledProviders = providers.filter(p => p.enabled);
    enabledProviders.forEach(provider => {
        // Routes that are triggered by the callbacks from each OAuth provider once
        // the user has authenticated successfully
        router.get(callbackURL.replace(':provider', provider.name), provider.authenticate, provider.callback(redirectUri));
    });

    // This custom middleware allows us to attach the socket id to the session
    // With that socket id we can send back the right user info to the right
    // socket
    router.use((req, res, next) => {
        req.session.socketId = req.query.socketId || uuid.v4();
        req.session.save(() => next());
    });

    // Routes that are triggered on the client
    enabledProviders.forEach(provider => {
        // Routes that are triggered by the callbacks from each OAuth provider once
        // the user has authenticated successfully
        router.get(`/${provider.name}`, provider.authenticate);
    });

    return router;
};

module.exports = getController;