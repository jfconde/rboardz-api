'use strict';

exports.seed = (knex, Promise) => knex('users_roles').del()
    .then(() => [
        {user_id: 1, role_id: 1},
        {user_id: 1, role_id: 3}
    ])
    .then(items => Promise.all(items.map(newItem =>
            knex
                .insert(newItem)
                .returning(['*'])
                .into('users_roles')
                .timeout(1000)
        )
    ))
    //.then(items => Promise.all(items.map(newItem => Model.create(newItem))))
    .catch(err => console.log('err: ', err));
