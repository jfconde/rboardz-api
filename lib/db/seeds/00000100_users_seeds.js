'use strict';
const {resetTableSeed} = require('../utils');
const modelName = 'User';

const {[modelName]: Model, knex} = require('../models/index');

exports.seed = (knex, Promise) => knex(Model.tableName).del()
    .then(() => resetTableSeed(Model.tableName, knex))
    .then(() => [
        {id: 1, username: 'testUser', email: 'test@gmail.com', password: 'test'}
    ])
    .then(items => Promise.all(items.map(newItem => Model.create(newItem))))
    .catch(err => console.log('err: ', err));
