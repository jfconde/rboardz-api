'use strict';
const {resetTableSeed} = require('../utils');
const modelName = 'Role';

const {[modelName]: Model, knex} = require('../models/index');

exports.seed = (knex, Promise) => knex(Model.tableName).del()
    .then(() => resetTableSeed(Model.tableName, knex))
    .then(() => [
        {id: 1, name: 'user'},
        {id: 2, name: 'moderator'},
        {id: 3, name: 'admin'}
    ])
    .then(items => Promise.all(items.map(newItem => Model.create(newItem))))
    .then(() => [
        {role_id: 1, permission_id: 1},
        {role_id: 2, permission_id: 1},
        {role_id: 2, permission_id: 2},
        {role_id: 2, permission_id: 3},
        {role_id: 2, permission_id: 4},
        {role_id: 3, permission_id: 1},
        {role_id: 3, permission_id: 2},
        {role_id: 3, permission_id: 3},
        {role_id: 3, permission_id: 4},
        {role_id: 3, permission_id: 5},
        {role_id: 3, permission_id: 6}
    ])
    .then(items => Promise.all(items.map(newItem =>
            knex
                .insert(newItem)
                .returning(['*'])
                .into('roles_permissions')
                .timeout(1000)
        )
    ))
    .catch(err => {
        console.log('err: ', err);
    });
