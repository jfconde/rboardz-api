'use strict';
const {resetTableSeed} = require('../utils');
const modelName = 'Permission';

const {[modelName]: Model, knex} = require('../models/index');

exports.seed = (knex, Promise) => knex('roles_permissions').del()
    .then(() => knex('users_roles').del())
    .then(() => knex(Model.tableName).del())
    .then(() => resetTableSeed(Model.tableName, knex))
    .then(() => [
        {id: 1, name: 'read_posts'},
        {id: 2, name: 'del_posts'},
        {id: 3, name: 'edit_posts'},
        {id: 4, name: 'moderate_posts'},
        {id: 5, name: 'manage_community'},
        {id: 6, name: 'manage_app'}
    ])
    .then(items => Promise.all(items.map(newItem => Model.create(newItem))))
    .catch(err => console.log('err: ', err));
