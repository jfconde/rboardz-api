'use strict';

const env = process.env.NODE_ENV || 'development';
const config = require(`../../config/config.${env.toLowerCase()}`);

module.exports = config.db;