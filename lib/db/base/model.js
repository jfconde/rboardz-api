const knex = require('../knex');
const {idName} = require('../tableDefinitions');
const {pickBy} = require('../../util/object');
const DEFAULT_TIMEOUT = 2500;

//----------------------------------------------------------------------
const pickValidData = (data = {}, fields = {}) => Object.keys(data).reduce(
    (res, key) => (
        (Array.isArray(fields) ? fields.includes(key) : !!fields[key]) && ({
            ...res,
            [key]: data[key]
        })) || res, {});

//----------------------------------------------------------------------

const model = (config = {}) => {
    const {
              tableName,
              name,
              fields,
              attributeWhitelist,
              timeout = DEFAULT_TIMEOUT
          } = config;

    const Model = function (_data) {
        let data = pickValidData(_data, fields);

        const set = (fieldName, value) => {
            if (typeof (fieldName) === 'object') {
                return Object.keys(fieldName).some(k => set(k, fieldName[k]));
            } else {
                if (!!fields[fieldName] && data[fieldName] !== value) {
                    data = {...data, [fieldName]: value};
                    return true;
                } else {
                    return false;
                }
            }
        };

        const get = (fieldName) => {
            if (Array.isArray(fieldName)) {
                return fieldName.map(f => get(f));
            } else {
                if (!!fields[fieldName]) {
                    return data[fieldName];
                } else {
                    return undefined;
                }
            }
        };

        return {
            get,
            set,
            get data() {
                return pickValidData(data, attributeWhitelist); // Avoid being modified from the outside.
            },
            get tableName() {
                return tableName;
            },
            get fields() {
                return fields;
            },
            get name() {
                return name;
            },
            get attributeWhitelist() {
                return [...attributeWhitelist];
            }
        };
    };

    Model.toString = () => `Model ${name} (table: ${tableName})`;

    Model.create = props => {
        delete props.id; // not allowed to set `id`
        return knex.insert(props)
            .returning(attributeWhitelist)
            .into(tableName)
            .timeout(timeout);
    };

    Model.findAll = () => knex.select(attributeWhitelist)
        .from(tableName)
        .timeout(timeout);

    Model.find = (filters = {}) => knex.select(attributeWhitelist)
        .from(tableName)
        .where(filters)
        .timeout(timeout);

    Model.findOne = (filters) => Model.find(filters)
        .then(results => {
            if (!Array.isArray(results)) return results;
            return results[0];
        });

    Model.findById = id => knex.select(attributeWhitelist)
        .from(tableName)
        .where({[idName]: id})
        .timeout(timeout);

    Model.destroyById = id => knex.del()
        .from(tableName)
        .where({[idName]: id})
        .timeout(timeout);

    Model.destroyBy = (filters = -1) => knex.del() // Use -1 to cause exception and avoid clearing large tables by accident.
        .from(tableName)
        .where(filters)
        .timeout(timeout);

    Model.update = (filters, values) => knex.where(filters)
        .update(values)
        .from(tableName)
        .timeout(timeout);


    Model.modelName = name;
    Model.tableName = tableName;


    return Model;
};

model.copyStatics = (from, to) => Object.keys(from).forEach(k => to[k] = from[k]);

module.exports = model;