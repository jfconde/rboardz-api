const tableNames = {
    User            : 'users',
    Role            : 'roles',
    Permission      : 'permissions',
    RolesPermissions: 'roles_permissions',
    Sessions        : 'sessions',
    UsersRoles      : 'users_roles'
};

const tableFields = {
    User: {
        username      : 'username',
        displayName   : 'display_name',
        provider      : 'provider',
        providerUserId: 'provider_user_id',
        password      : 'password',
        email         : 'email'
    },

    Role: {
        name       : 'name',
        description: 'description'
    },

    Permission: {
        name       : 'name',
        description: 'description'
    },

    RolesPermissions: {
        roleId      : 'role_id',
        permissionId: 'permission_id'
    },

    UsersRoles: {
        roleId: 'role_id',
        userId: 'user_id'
    },

    Sessions: {
        sid    : 'sid',
        expires: 'expires',
        data   : 'data'
    }
};

const idName = 'id';
const timeStamps = {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
};

module.exports = {
    idName,
    timeStamps,
    tableNames,
    tableFields
};