const resetTableSeed = (tableName, knex) => {
    const client = knex.client.config.client;
    const name = tableName.toLowerCase();
    let resetSequenceCommand = '';

    if (client === 'sqlite3') {
        resetSequenceCommand = `UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='${name}';`;
    } else if (client === 'postgres' || client === 'pg') {
        resetSequenceCommand = `ALTER SEQUENCE ${name}_id_seq RESTART WITH 1`;
    }

    return knex.raw(resetSequenceCommand);
};

module.exports = {
    resetTableSeed
};