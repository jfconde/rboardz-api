'use strict';

const env = process.env.NODE_ENV || 'development';
const config = require(`../../config/config.${env.toLowerCase()}`);
const knex = require('knex')(config.db);

module.exports = knex;
