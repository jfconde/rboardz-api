'use strict';
const Model = require('../base/model');
const {tableNames, tableFields} = require('../tableDefinitions');

const Session = Model({
    tableName: tableNames.Sessions,
    name: 'Session',
    fields: tableFields.Sessions,
    attributeWhitelist: Object.keys(tableFields.Sessions)
});

module.exports = Session;
