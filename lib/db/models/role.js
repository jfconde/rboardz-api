'use strict';
const Model = require('../base/model');
const {tableNames, tableFields} = require('../tableDefinitions');

const Role = Model({
    tableName: tableNames.Role,
    name: 'Role',
    fields: tableFields.Role,
    attributeWhitelist: Object.keys(tableFields.Role)
});

module.exports = Role;
