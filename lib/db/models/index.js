'use strict';

const fs = require('fs');
const path = require('path');
const knex = require('../index');

const getModelFiles = dir => fs.readdirSync(dir)
    .filter(file => (file.indexOf('.') !== 0) && (file !== 'index.js'))
    .map(file => path.join(dir, file));

// Gather up all model files (i.e., any file present in the current directory
// that is not this file) and export them as properties of an object such that
// they may be imported using destructuring like
// `const { MyModel } = require('./models')` where there is a model named
// `MyModel` present in the exported object of gathered models.
const files = getModelFiles(__dirname);

const models = files.reduce((modelsObj, filename) => {
    const model = require(filename);

    if (model) modelsObj[model.modelName] = model;
    return modelsObj;
}, {});

models.knex = knex;

module.exports = models;
