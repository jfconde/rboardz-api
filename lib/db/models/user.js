'use strict';

const bcrypt = require('bcrypt');
const Model = require('../base/model');
const Role = require('./role');
const Permission = require('./permission');

const name = 'User';
const {idName, tableNames, tableFields, timeStamps} = require('../tableDefinitions');
const fields = tableFields[name];

// Bcrypt functions used for hashing password and later verifying it.
const SALT_ROUNDS = 10;
const hashPassword = password => bcrypt.hash(password, SALT_ROUNDS);
const verifyPassword = (password, hash) => bcrypt.compare(password, hash);

// Always perform this logic before saving to db. This includes always hashing
// the password field prior to writing so it is never saved in plain text.
const beforeSave = user => {
    if (!user[fields.password]) return Promise.resolve(user);

    // `password` will always be hashed before being saved.
    return hashPassword(user[fields.password])
        .then(hash => ({...user, [fields.password]: hash}))
        .catch(err => `Error hashing password: ${err}`);
};

const baseUserModel = Model({
    tableName         : tableNames.User,
    name              : 'User',
    fields            : {...tableFields.User, timeStamps},
    attributeWhitelist: Object.values(tableFields.User).filter(f => f !== tableFields.User.password)
});

const augmentedUserModel = function (data) {
    const base = baseUserModel(data);
    const create = data => beforeSave(data).then(d => base.create(d));

    return {
        ...base,
        create
    };
};

Model.copyStatics(baseUserModel, augmentedUserModel);

augmentedUserModel.verify = (username, password) => {
    const matchErrorMsg = 'Username or password do not match';
    knex.select()
        .from(augmentedUserModel.tableName)
        .where({[fields.username]: username})
        .timeout(augmentedUserModel.timeout)
        .then(user => {
            if (!user) throw matchErrorMsg;
            return user;
        })
        .then(user =>
            Promise.all([user, verifyPassword(password, user[fields.password])])
        )
        .then(([user, isMatch]) => {
            if (!isMatch) throw matchErrorMsg;
            return user;
        });
};

module.exports = augmentedUserModel;

/*
const getRolesForUserId = userId => knex
    .select()
    .from(tableNames.UsersRoles)
    .where({[tableFields.UsersRoles.userId]: userId})
    .leftJoin(
        Role.tableName,
        `${tableNames.UsersRoles}.${tableFields.UsersRoles.roleId}`,
        `${Role.tableName}.${idName}`
    );

const getRolesAndPermissions = userId => getRolesForUserId(userId)
    .then(roles => knex
        .select()
        .from(tableNames.RolesPermissions)
        .where(builder =>
            builder.whereIn(
                `${tableNames.RolesPermissions}.${tableFields.UsersRoles.roleId}`,
                roles.map(r => r.id)
            )
        )
        .leftJoin(
            Permission.tableName,
            `${tableNames.RolesPermissions}.${tableFields.RolesPermissions.permissionId}`,
            `${tableNames.Permission}.${idName}`
        )
        .then((permissions) => Promise.resolve({roles, permissions}))
    );
    */