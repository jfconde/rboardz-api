'use strict';
const Model = require('../base/model');
const {tableNames, tableFields} = require('../tableDefinitions');

const Permission = Model({
    tableName: tableNames.Permission,
    name: 'Permission',
    fields: tableFields.Permission,
    attributeWhitelist: Object.keys(tableFields.Permission)
});

module.exports = Permission;
