const {idName, tableNames, tableFields} = require('../tableDefinitions');

const fields = tableFields.UsersRoles;

exports.up = knex => {
    return knex.schema.createTable(tableNames.UsersRoles, t => {
        t.integer(fields.roleId).references(`${tableNames.Role}.${idName}`).notNull();
        t.integer(fields.userId).references(`${tableNames.User}.${idName}`).notNull();
        t.unique([fields.roleId, fields.userId]);
    });
};

exports.down = knex => {
    return knex.schema.dropTable(tableNames.UsersRoles);
};
