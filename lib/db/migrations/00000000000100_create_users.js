const {idName, timeStamps, tableNames, tableFields} = require('../tableDefinitions');

const fields = tableFields.User;

exports.up = knex => {
    return knex.schema.createTable(tableNames.User, t => {
        t.increments(idName).primary().unsigned();
        t.string(fields.username).unique().index();
        t.string(fields.password);
        t.string(fields.email).unique().index();
        t.string(fields.provider, 20);
        t.string(fields.providerUserId);
        t.string(fields.displayName, 64);

        t.timestamp(timeStamps.createdAt).defaultTo(knex.fn.now());
        t.timestamp(timeStamps.updatedAt).defaultTo(knex.fn.now());
    });
};

exports.down = knex => {
    return knex.schema.dropTable(tableNames.User);
};
