const {idName, timeStamps, tableNames, tableFields} = require('../tableDefinitions');

const fields = tableFields.RolesPermissions;

exports.up = knex => {
    return knex.schema.createTable(tableNames.RolesPermissions, t => {
        t.integer(fields.roleId).references(`${tableNames.Role}.${idName}`).notNull();
        t.integer(fields.permissionId).references(`${tableNames.Permission}.${idName}`).notNull();
        t.unique([fields.roleId, fields.permissionId]);
    });
};

exports.down = knex => {
    return knex.schema.dropTable(tableNames.RolesPermissions);
};
