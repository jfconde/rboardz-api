const {idName, tableNames, tableFields} = require('../tableDefinitions');
const fields = tableFields.Sessions;

exports.up = knex => {
    return knex.schema.createTable(tableNames.Sessions, t => {
        t.string(fields.sid).notNull().primary().unique();
        t.dateTime(fields.expires).notNull();
        t.text(fields.data);
    });
};

exports.down = knex => {
    return knex.schema.dropTable(tableNames.Sessions);
};
