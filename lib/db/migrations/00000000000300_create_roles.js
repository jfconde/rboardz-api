const {idName, tableNames, tableFields} = require('../tableDefinitions');

const fields = tableFields.Role;

exports.up = knex => {
    return knex.schema.createTable(tableNames.Role, t => {
        t.increments(idName).primary().unsigned();
        t.string(fields.name).unique().notNull();
        t.string(fields.description, 255);
    });
};

exports.down = knex => {
    return knex.schema.dropTable(tableNames.Role);
};

