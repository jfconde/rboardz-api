const {idName, tableNames, tableFields} = require('../tableDefinitions');

const fields = tableFields.Permission;

exports.up = knex => {
    return knex.schema.createTable(tableNames.Permission, t => {
        t.increments(idName).primary().unsigned();
        t.string(fields.name).unique();
        t.string(fields.description);
    });
};

exports.down = knex => {
    return knex.schema.dropTable(tableNames.Permission);
};
