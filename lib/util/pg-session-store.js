'use strict';

const util = require('util');
const oneDay = 86400;
const {tableFields} = require('../db/tableDefinitions');

const currentTimestamp = function () {
    return Math.ceil(Date.now() / 1000);
};

module.exports = function (session) {
    const Store = session.Store || session.session.Store;

    const PGStore = function (SessionModel, options) {
        options = options || {};
        Store.call(this, options);

        this.ttl = options.ttl;
        this.SessionModel = SessionModel;

        this.errorLog = options.errorLog || console.error.bind(console);

        if (options.pruneSessionInterval === false) {
            this.pruneSessionInterval = false;
        } else {
            this.pruneSessionInterval = (options.pruneSessionInterval || 60) * 1000;
            setImmediate(function pruneSessions() {
                this.pruneSessions();
            }.bind(this));
        }
    };

    /**
     * Inherit from `Store`.
     */

    util.inherits(PGStore, Store);

    /**
     * Closes the session store
     *
     * Currently only stops the automatic pruning, if any, from continuing
     *
     * @access public
     */

    PGStore.prototype.close = function () {
        this.closed = true;

        if (this.pruneTimer) {
            clearTimeout(this.pruneTimer);
            this.pruneTimer = undefined;
        }
    };

    /**
     * Does garbage collection for expired session in the database
     *
     * @param {Function} [fn] - standard Node.js callback called on completion
     * @access public
     */

    PGStore.prototype.pruneSessions = function (fn) {
        this.SessionModel.destroyBy(builder =>
            builder.where(tableFields.Sessions.expires, '<', new Date())
        ).then(_ => {
            if (fn && typeof fn === 'function') {
                return fn();
            }

            if (this.pruneSessionInterval && !this.closed) {
                if (this.pruneTimer) {
                    clearTimeout(this.pruneTimer);
                }
                this.pruneTimer = setTimeout(this.pruneSessions.bind(this, true), this.pruneSessionInterval);
                this.pruneTimer.unref();
            }
        }).catch(err => {
            this.errorLog('Failed to prune sessions:', err.message);
        });
    };


    /**
     * Figure out when a session should expire
     *
     * @param {Number} [maxAge] - the maximum age of the session cookie
     * @return {Number} the unix timestamp, in seconds
     * @access private
     */

    PGStore.prototype.getExpireTime = function (maxAge) {
        let ttl = this.ttl;

        ttl = ttl || (typeof maxAge === 'number' ? maxAge / 1000 : oneDay);
        ttl = Math.ceil(ttl + currentTimestamp());

        return ttl;
    };


    /**
     * Attempt to fetch session by the given `sid`.
     *
     * @param {String} sid – the session id
     * @param {Function} fn – a standard Node.js callback returning the parsed session object
     * @access public
     */

    PGStore.prototype.get = function (sid, fn) {
        const dataIndex = tableFields.Sessions.data;
        this.SessionModel.find({[tableFields.Sessions.sid]: sid}).then(sessions => {
            const session = sessions && sessions.length && sessions[0];
            if (!session) {
                return fn();
            }
            try {
                return fn(null, (typeof session[dataIndex] === 'string') ? JSON.parse(session[dataIndex]) : session[dataIndex]);
            } catch (e) {
                return this.destroy(sid, fn);
            }
        }).catch(e => fn(e));
    };

    /**
     * Commit the given `sess` object associated with the given `sid`.
     *
     * @param {String} sid – the session id
     * @param {Object} sess – the session object to store
     * @param {Function} fn – a standard Node.js callback returning the parsed session object
     * @access public
     */

    PGStore.prototype.set = function (newSid, sess, fn) {
        const expireTime = this.getExpireTime(sess.cookie.maxAge);
        const {sid, expires, data} = tableFields.Sessions;
        this.SessionModel.findOne({
            [sid]: newSid
        }).then(user => {
            if (user) {
                this.touch(newSid, sess, fn);
            } else {
                this.SessionModel.create({
                    [sid]    : newSid,
                    [expires]: new Date(expireTime * 1000),
                    [data]   : sess
                }).then(session =>
                    fn()).catch(e => fn(e));
            }
        }).catch(e => fn(e));
    };

    /**
     * Destroy the session associated with the given `sid`.
     *
     * @param {String} sid – the session id
     * @access public
     */

    PGStore.prototype.destroy = function (sid, fn) {
        this.SessionModel.destroyBy({[tableFields.Sessions.sid]: sid}).then(() => fn()).catch(e => fn(e));
    };

    /**
     * Touch the given session object associated with the given session ID.
     *
     * @param {String} sid – the session id
     * @param {Object} sess – the session object to store
     * @param {Function} fn – a standard Node.js callback returning the parsed session object
     * @access public
     */

    PGStore.prototype.touch = function (sid, sess, fn) {
        const expireTime = this.getExpireTime(sess.cookie.maxAge);
        this.SessionModel.update(
            {[tableFields.Sessions.sid]: sid},
            {[tableFields.Sessions.data]: sess},
            {[tableFields.Sessions.expires]: new Date(expireTime * 1000)}
        ).then(() => fn()).catch(e => fn(e));
    };

    return PGStore;
};