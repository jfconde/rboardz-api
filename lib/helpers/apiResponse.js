const errorCodes = require('./error').codes;
const CODE_SUCCESS = 200; // <- This should not be necessary, but whatever
const env = process.env.NODE_ENV || 'development';

const apiResponse = function (config = {}) {
    const {data = null, error = null, paging = null, reqId} = config;
    let isCollection = data && Array.isArray(data);
    let errorObj = null;
    if (error) {
        errorObj = {
            code : error.code,
            msg  : error.msg,
            trace: env === 'development' ? error.trace : undefined
        };
    }

    return {
        data,
        error     : errorObj,
        paging    : isCollection ? paging || null : null, // We could conditionally set this, but I prefer static templates.
        statusCode: error ? (errorCodes[errorObj.code] || errorCodes['GENERIC_ERROR']) : CODE_SUCCESS
        // Room in the future to add more stuff like caching, redirects, etc.
    };
};

const withApiResponse = (bodyFunction) => {
    return async (req, res, next) => {
        let resultObj = null;
        let response = null;

        try {
            resultObj = await bodyFunction(req, res, next);
            response = apiResponse(resultObj);
        } catch (err) {
            const isProperError = !!err.code && !!err.msg;
            response = apiResponse({
                error: {
                    code : isProperError ? err.code : 'GENERIC_ERROR',
                    msg  : isProperError ? err.msg : 'Server error',
                    trace: !isProperError ? err : undefined // Only trace internal errors in dev
                }
            });
        }
        var x = response;
        //res.status(response.statusCode);
        res.send(response);
    };
};

module.exports = {
    apiResponse,
    withApiResponse
};