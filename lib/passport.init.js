const passport = require('passport');
const {Strategy: TwitterStrategy} = require('passport-twitter');
const {OAuth2Strategy: GoogleStrategy} = require('passport-google-oauth');
const {Strategy: FacebookStrategy} = require('passport-facebook');
const {Strategy: GithubStrategy} = require('passport-github');
const db = require('./db/models');
const {tableFields} = require('./db/tableDefinitions');

const availableProviders = [
    {name: 'facebook', strategy: FacebookStrategy},
    {name: 'github', strategy: GithubStrategy},
    {name: 'google', strategy: GoogleStrategy},
    {name: 'twitter', strategy: TwitterStrategy}
];

const fn = (providersConfig = {}, host) => {
    const {callbackURL, path} = providersConfig;
    // Allowing passport to serialize and deserialize users into sessions
    passport.serializeUser((user, cb) => {
        cb(null, user);
    });
    passport.deserializeUser((obj, cb) => {
        const {provider, id} = obj;
        db.User.findOne({
            [tableFields.User.provider]      : provider,
            [tableFields.User.providerUserId]: id
        }).then((user) => {
            if (user) {
                cb(null, {...user});
            } else {
                db.User.create({
                    [tableFields.User.provider]      : provider,
                    [tableFields.User.providerUserId]: id
                }).then((users) => {
                    const user = users.length && users[0];
                    cb(null, {...user});
                }).catch((err) => cb(null, null));
            }
        }).catch((err) => cb(null, null));
    });

    // The callback that is invoked when an OAuth provider sends back user
    // information. Normally, you would save the user to the database
    // in this callback and it would be customized for each provider
    const callback = (accessToken, refreshToken, profile, cb) => {
        const {provider, id} = profile;
        db.User.findOne({
            [tableFields.User.provider]      : provider,
            [tableFields.User.providerUserId]: id
        }).then((user) => {
            if (user) {
                cb(null, profile);
            } else {
                db.User.create({
                    [tableFields.User.provider]      : provider,
                    [tableFields.User.providerUserId]: id
                }).then((users) => {
                    const user = users.length && users[0];
                    cb(null, profile);
                }).catch((err) => {
                    cb(null, null);
                });
            }
        });
    };


    availableProviders.forEach(provider => {
        const {name, strategy} = provider;
        const {enabled, ...config} = providersConfig[name] || {};
        if (enabled) {
            passport.use(new strategy({
                ...config,
                callbackURL: (`${host}${path}${callbackURL.replace(':provider', name)}`)
            }, callback));
        }
    });
};

const strategies = availableProviders.map(p => p.name);

module.exports = {fn, strategies};