require('dotenv').config();
const express = require('express');
const fs = require('fs');
const https = require('https');
const http = require('http');
const passport = require('passport');
const session = require('express-session');
const sharedSession = require('express-socket.io-session');
const cors = require('cors');
const socketIO = require('socket.io');
const passportInit = require('./lib/passport.init');
const env = process.env.NODE_ENV || 'development';
const app = express();
const routes = require('./lib/routes');
const pgSessionStore = require('./lib/util/pg-session-store');
const config = require(`./config/config.${env.toLowerCase()}`);
const SessionModel = require('./lib/db/models').Session;

const buildServer = (config) => {
    let server;
    // If we are in production we are already running in https
    if (env === 'production') {
        server = http.createServer(app);
    }
    // We are not in production so load up our certificates to be able to
    // run the server in https mode locally
    else {
        const certOptions = {
            key : fs.readFileSync(config.httpsCert.key),
            cert: fs.readFileSync(config.httpsCert.cert)
        };
        server = https.createServer(certOptions, app);
    }

    // Setup for passport and to accept JSON objects
    app.use(express.json());
    const appSession = session({
        name             : 'app.sessId',
        resave           : true,
        saveUninitialized: false,
        secret           : process.env.SESSION_SECRET || config.sessionSecret,
        secure           : 'auto',
        store            : new (pgSessionStore(session))(SessionModel, {ttl: 60 * 10/*seconds*/})
    });
    app.use(appSession);

    // Accept requests from our client
    app.use(cors({
        origin: config.clientOrigin
    }));

    app.use(passport.initialize());
    app.use(passport.session());
    passportInit.fn(config.authProviders, config.clientOrigin);

    // Connecting sockets to the server and adding them to the request
    // so that we can access them later in the controller
    const io = socketIO(server);
    app.set('io', io);
    io.use(sharedSession(appSession, {autoSave: true}));
    io.sockets.on('connection', function (socket) {
        socket.on('join', function (room) {
            socket.join(room);
        });
    });

    // Catch a start up request so that a sleepy Heroku instance can
    app.get('/wake-up', (req, res) => {
        res.send('👍' + req.user);
    });
    app.use(routes({
        apiPath        : config.apiPath,
        authPath       : config.authProviders.path,
        authCallbackURL: config.authProviders.callbackURL,
        redirectUri    : config.authProviders.redirectUri
    }));

    // Direct other requests to the auth router
    //app.use(config.authProviders.path, authRouter(config.authProviders.callbackURL));
    return server;
};

const port = process.env.PORT || config.port;
const index = buildServer(config);

index.listen(port, () => {
    console.log(`listening on port ${port}.`);
});
